################################################################################
# [MA-RISCV] Kernels
################################################################################

############################################################
1. Matrix Multiplication
############################################################
DESCRIPTION:
Vectorial matrix multiplication

EXECUTE:
> ./hpc.matmul

############################################################
2. Wavefront Alignment Edit
############################################################
DESCRIPTION:
Aligns two strings of 512 characters each using the Wavefront 
Alignment Algorithm (WFA) for edit distance.

EXECUTE:
> ./wfe.align <REPETITIONS>
> ./wfe.align 100000

############################################################
3. Wavefront Alignment Gap-Affine 
############################################################
DESCRIPTION:
Executes the compute-next() step for string alignment using
the Wavefront Alignment algorithm (WFA) for Gap-Affine distance.

EXECUTE:
> ./wfa.compute <REPETITIONS> <VECTOR_LENGTH>
> ./wfa.compute 100000 10000

############################################################
4. Post-Quantum Computing. MCeliece Cryptosystem
############################################################

DESCRIPTION:
In cryptography, the McEliece cryptosystem is an asymmetric encryption 
algorithm developed in 1978 by Robert McEliece. It was the first such 
scheme to use randomization in the encryption process. The algorithm has
never gained much acceptance in the cryptographic community, but is a 
candidate for "post-quantum cryptography", as it is immune to attacks 
using Shor's algorithm and – more generally – measuring coset states 
using Fourier sampling.

EXECUTE:
> ./pqs.mceliece <REPETITIONS>
> ./pqs.mceliece 10