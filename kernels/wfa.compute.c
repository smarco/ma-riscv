/*
 *                             The MIT License
 *
 * Wavefront Alignments Algorithms
 * Copyright (c) 2017 by Santiago Marco-Sola  <santiagomsola@gmail.com>
 *
 * This file is part of Wavefront Alignments Algorithms.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * PROJECT: Wavefront Alignments Algorithms
 * AUTHOR(S): Santiago Marco-Sola <santiagomsola@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>

/*
 * Types
 */
typedef int32_t awf_offset_t;

/*
 * Support functions
 */
#define MAX(a,b) (((a)>=(b))?(a):(b))

/*
 * Compute wavefront offsets
 */
__attribute__((noinline))
int affine_wavefronts_compute_next(
    awf_offset_t* const m_gap_offsets,
    awf_offset_t* const i_ext_offsets,
    awf_offset_t* const d_ext_offsets,
    awf_offset_t* const m_sub_offsets,
    awf_offset_t* const out_ioffsets,
    awf_offset_t* const out_doffsets,
    awf_offset_t* const out_moffsets,
    const int wf_length) {
  // Compute score wavefronts (core)
  int k;
  //#pragma GCC ivdep
  #pragma clang loop vectorize(enable)
  for (k=0;k<wf_length;++k) {
    // Update I
    const awf_offset_t ins_g = m_gap_offsets[k-1];
    const awf_offset_t ins_i = i_ext_offsets[k-1];
    const awf_offset_t ins = MAX(ins_g,ins_i) + 1;
    out_ioffsets[k] = ins;
    // Update D
    const awf_offset_t del_g = m_gap_offsets[k+1];
    const awf_offset_t del_d = d_ext_offsets[k+1];
    const awf_offset_t del = MAX(del_g,del_d);
    out_doffsets[k] = del;
    // Update M
    const awf_offset_t sub = m_sub_offsets[k] + 1;
    out_moffsets[k] = MAX(del,MAX(sub,ins));
  }
  return out_moffsets[wf_length-1];
}
/*
 * Init
 */
static void wavefronts_init(
    awf_offset_t* const wavefront,
    const int wf_length) {
  int k;
  for (k=0;k<wf_length;++k) {
    wavefront[k] = rand(); // [0,RAND_MAX]
  }
}
/*
 * Main
 */
int main(int argc,char* argv[]) {
  // Parameters
  const int REPS = (argc>=2) ? atoi(argv[1]) : 100000;
  const int WF_LENGTH = (argc>=3) ? atoi(argv[2]) : 10000;

  // Allocate
  awf_offset_t* const m_gap_offsets = malloc(WF_LENGTH*sizeof(awf_offset_t));
  awf_offset_t* const i_ext_offsets = malloc(WF_LENGTH*sizeof(awf_offset_t));
  awf_offset_t* const d_ext_offsets = malloc(WF_LENGTH*sizeof(awf_offset_t));
  awf_offset_t* const m_sub_offsets = malloc(WF_LENGTH*sizeof(awf_offset_t));
  awf_offset_t* const out_ioffsets = malloc(WF_LENGTH*sizeof(awf_offset_t));
  awf_offset_t* const out_doffsets = malloc(WF_LENGTH*sizeof(awf_offset_t));
  awf_offset_t* const out_moffsets = malloc(WF_LENGTH*sizeof(awf_offset_t));

  // Init
  srand(time(NULL));   // Initialization, should only be called once.
  wavefronts_init(m_gap_offsets,WF_LENGTH);
  wavefronts_init(i_ext_offsets,WF_LENGTH);
  wavefronts_init(d_ext_offsets,WF_LENGTH);
  wavefronts_init(m_sub_offsets,WF_LENGTH);

  // Wavefront compute-nex()
  int i, check=0;
  for (i=0;i<REPS;++i) {
    check += affine_wavefronts_compute_next(
        m_gap_offsets,i_ext_offsets,
        d_ext_offsets,m_sub_offsets,
        out_ioffsets,out_doffsets,out_moffsets,WF_LENGTH);
  }

  // Free
  free(m_gap_offsets);
  free(i_ext_offsets);
  free(d_ext_offsets);
  free(m_sub_offsets);
  free(out_ioffsets);
  free(out_doffsets);
  free(out_moffsets);

  return 0;
}

