/*
 * PROJECT: Post-Quantum Computing. MCeliece Criptosystem
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>

#define GFBITS 13
#define SYS_N 6688
#define SYS_T 128

#define GFMASK ((1 << GFBITS) - 1)

#define PK_NROWS (SYS_T*GFBITS)
#define PK_NCOLS (SYS_N - PK_NROWS)
#define PK_ROW_BYTES ((PK_NCOLS + 7)/8)

typedef char bit;
typedef uint16_t gf;

/*
 * Bit manipulations
 */
static bit is_smaller_63b(uint64_t a, uint64_t b) {
  uint64_t ret = 0;

  ret = a - b;
  ret >>= 63;

  return ret;
}
static void cswap_63b(uint64_t *x,uint64_t *y,bit swap) {
  uint64_t m;
  uint64_t d;

  m = swap;
  m = 0 - m;

  d = (*x ^ *y);
  d &= m;
  *x ^= d;
  *y ^= d;
}
static void minmax_63b(uint64_t *x, uint64_t *y) {
  bit m;

  m = is_smaller_63b(*y, *x);
  cswap_63b(x, y, m);
}
static void merge_63b(int n,uint64_t x[n],int step) {
  int i;
  if (n == 1)
    minmax_63b(&x[0],&x[step]);
  else {
    merge_63b(n / 2,x,step * 2);
    merge_63b(n / 2,x + step,step * 2);
    for (i = 1;i < 2*n-1;i += 2)
      minmax_63b(&x[i * step],&x[(i + 1) * step]);
  }
}
void sort_63b(int n, uint64_t x[n]) {
  if (n <= 1) return;
  sort_63b(n/2,x);
  sort_63b(n/2,x + n/2);
  merge_63b(n/2,x,1);
}
gf bitrev(gf a) {
  a = ((a & 0x00FF) << 8) | ((a & 0xFF00) >> 8);
  a = ((a & 0x0F0F) << 4) | ((a & 0xF0F0) >> 4);
  a = ((a & 0x3333) << 2) | ((a & 0xCCCC) >> 2);
  a = ((a & 0x5555) << 1) | ((a & 0xAAAA) >> 1);

  return a >> 3;
}
uint16_t load2(const unsigned char *src) {
  uint16_t a;

  a = src[1];
  a <<= 8;
  a |= src[0];

  return a & GFMASK;
}
/*
 * GF Mul
 */
gf gf_mul(gf in0, gf in1) {
  int i;

  uint64_t tmp;
  uint64_t t0;
  uint64_t t1;
  uint64_t t;

  t0 = in0;
  t1 = in1;

  tmp = t0 * (t1 & 1);

  for (i = 1; i < GFBITS; i++)
    tmp ^= (t0 * (t1 & (1 << i)));

  //

  t = tmp & 0x1FF0000;
  tmp ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

  t = tmp & 0x000E000;
  tmp ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

  return tmp & GFMASK;
}
gf gf_sq2mul(gf in, gf m) {
  int i;

  uint64_t x;
  uint64_t t0;
  uint64_t t1;
  uint64_t t;

  const uint64_t M[] = {0x1FF0000000000000,
                  0x000FF80000000000,
                  0x000007FC00000000,
                        0x00000003FE000000,
                        0x0000000001FE0000,
                        0x000000000001E000};

  t0 = in;
  t1 = m;

  x = (t1 << 18) * (t0 & (1 << 6));

  t0 ^= (t0 << 21);

  x ^= (t1 * (t0 & (0x010000001)));
  x ^= (t1 * (t0 & (0x020000002))) << 3;
  x ^= (t1 * (t0 & (0x040000004))) << 6;
  x ^= (t1 * (t0 & (0x080000008))) << 9;
  x ^= (t1 * (t0 & (0x100000010))) << 12;
  x ^= (t1 * (t0 & (0x200000020))) << 15;

  for (i = 0; i < 6; i++)
  {
    t = x & M[i];
    x ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);
  }

  return x & GFMASK;
}
gf gf_sqmul(gf in, gf m) {
  int i;

  uint64_t x;
  uint64_t t0;
  uint64_t t1;
  uint64_t t;

  const uint64_t M[] = {0x0000001FF0000000,
                        0x000000000FF80000,
                        0x000000000007E000};

  t0 = in;
  t1 = m;

  x = (t1 << 6) * (t0 & (1 << 6));

  t0 ^= (t0 << 7);

  x ^= (t1 * (t0 & (0x04001)));
  x ^= (t1 * (t0 & (0x08002))) << 1;
  x ^= (t1 * (t0 & (0x10004))) << 2;
  x ^= (t1 * (t0 & (0x20008))) << 3;
  x ^= (t1 * (t0 & (0x40010))) << 4;
  x ^= (t1 * (t0 & (0x80020))) << 5;

  for (i = 0; i < 3; i++)
  {
    t = x & M[i];
    x ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);
  }

  return x & GFMASK;
}
gf gf_sq2(gf in) {
  int i;

  const uint64_t B[] = {0x1111111111111111,
                        0x0303030303030303,
                        0x000F000F000F000F,
                        0x000000FF000000FF};

  const uint64_t M[] = {0x0001FF0000000000,
                        0x000000FF80000000,
                        0x000000007FC00000,
                        0x00000000003FE000};

  uint64_t x = in;
  uint64_t t;

  x = (x | (x << 24)) & B[3];
  x = (x | (x << 12)) & B[2];
  x = (x | (x << 6)) & B[1];
  x = (x | (x << 3)) & B[0];

  for (i = 0; i < 4; i++)
  {
    t = x & M[i];
    x ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);
  }

  return x & GFMASK;
}
gf gf_frac(gf den, gf num) {
  gf tmp_11;
  gf tmp_1111;
  gf out;

  tmp_11 = gf_sqmul(den, den); // ^11
  tmp_1111 = gf_sq2mul(tmp_11, tmp_11); // ^1111
  out = gf_sq2(tmp_1111);
  out = gf_sq2mul(out, tmp_1111); // ^11111111
  out = gf_sq2(out);
  out = gf_sq2mul(out, tmp_1111); // ^111111111111

  return gf_sqmul(out, num); // ^1111111111110 = ^-1
}
gf gf_inv(gf den) {
  return gf_frac(den, ((gf) 1));
}
/*
 * Polynomial Root
 *   input: polynomial f and field element a
 *   return f(a)
 */
void root(gf *f, gf *a, gf* out) {
  int i, j, k;
  gf r, r_tmp;
  uint32_t tmp;
  uint32_t t0;
  uint32_t t1;
  uint32_t t;

  for (k = 0; k < SYS_N; k++) {
    r = f[ SYS_T ];
    for (i = SYS_T-1; i >= 0; i--) {
      t0 = r;
      t1 = a[k];

      tmp = t0 * (t1 & 1);

      for (j = 1; j < GFBITS; j++)
        tmp ^= (t0 * (t1 & (1 << j)));

      t = tmp & 0x1FF0000;
      tmp ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

      t = tmp & 0x000E000;
      tmp ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

      r_tmp = tmp & ((1 << GFBITS)-1);

      r = r_tmp ^ f[i];
    }

    out[k]=r;
  }
}
/*
 * Public Key Generation
 *   input: secret key sk
 *   output: public key pk
 */
int pk_gen(
  unsigned char * pk,
  unsigned char * sk,
  uint32_t * perm) {

  int i, j, k;
  int row, c;

  uint64_t buf[ 1 << GFBITS ];

  unsigned char mat[ GFBITS * SYS_T ][ SYS_N/8 ];
  unsigned char mask;
  unsigned char b;

  gf g[ SYS_T+1 ]; // Goppa polynomial
  gf L[ SYS_N ];   // support
  gf inv[ SYS_N ];

  g[ SYS_T ] = 1;
  for (i = 0; i < SYS_T; i++) {
    g[i] = load2(sk);
    g[i] &= GFMASK;
    sk += 2;
  }
  for (i = 0; i < (1 << GFBITS); i++) {
    buf[i] = perm[i];
    buf[i] <<= 31;
    buf[i] |= i;
  }

  sort_63b(1 << GFBITS, buf);

  for (i = 0; i < (1 << GFBITS); i++) perm[i] = buf[i] & GFMASK;
  for (i = 0; i < SYS_N;         i++) L[i] = bitrev(perm[i]);

  // filling the matrix
  root(g, L, inv);

  for (i = 0; i < SYS_N; i++) {
    inv[i] = gf_inv(inv[i]);
  }
  for (i = 0; i < PK_NROWS; i++) {
    for (j = 0; j < SYS_N/8; j++) {
      mat[i][j] = 0;
    }
  }
  for (i = 0; i < SYS_T; i++) {
    for (j = 0; j < SYS_N; j+=8) {
      for (k = 0; k < GFBITS;  k++) {
        b  = (inv[j+7] >> k) & 1; b <<= 1;
        b |= (inv[j+6] >> k) & 1; b <<= 1;
        b |= (inv[j+5] >> k) & 1; b <<= 1;
        b |= (inv[j+4] >> k) & 1; b <<= 1;
        b |= (inv[j+3] >> k) & 1; b <<= 1;
        b |= (inv[j+2] >> k) & 1; b <<= 1;
        b |= (inv[j+1] >> k) & 1; b <<= 1;
        b |= (inv[j+0] >> k) & 1;
        mat[ i*GFBITS + k ][ j/8 ] = b;
      }
    }
    for (j = 0; j < SYS_N; j++) {
      inv[j] = gf_mul(inv[j], L[j]);
    }
  }
  // gaussian elimination
  for (i = 0; i < (GFBITS * SYS_T + 7) / 8; i++) {
    for (j = 0; j < 8; j++) {
      row = i*8 + j;

      if (row >= GFBITS * SYS_T) break;

      for (k = row + 1; k < GFBITS * SYS_T; k++)
      {
        mask = mat[ row ][ i ] ^ mat[ k ][ i ];
        mask >>= j;
        mask &= 1;
        mask = -mask;
        #pragma clang loop vectorize(enable)
        for (c = 0; c < SYS_N/8; c++)
          mat[ row ][ c ] ^= mat[ k ][ c ] & mask;
      }

      if ( ((mat[ row ][ i ] >> j) & 1) == 0 ) { // return if not systematic
        return -1;
      }

      for (k = 0; k < GFBITS * SYS_T; k++)
      {
        if (k != row)
        {
          mask = mat[ k ][ i ] >> j;
          mask &= 1;
          mask = -mask;
          #pragma clang loop vectorize(enable)
          for (c = 0; c < SYS_N/8; c++)
            mat[ k ][ c ] ^= mat[ row ][ c ] & mask;
        }
      }
    }
  }

  for (i = 0; i < PK_NROWS; i++)
    memcpy(pk + i*PK_ROW_BYTES, mat[i] + PK_NROWS/8, PK_ROW_BYTES);

  return 0;
}
/*
 * Init
 */
void random_init(
    uint8_t* const buffer,
    const int buffer_size) {
  int k;
  for (k=0;k<buffer_size;++k) {
    buffer[k] = rand() % 255; // [0,RAND_MAX]
  }
}
/*
 * Main
 */
int main(int argc,char* argv[]) {
  // Parameters
  const int REPS = (argc>=2) ? atoi(argv[1]) : 10;

  // Allocate
  const int pk_size = PK_NROWS * PK_ROW_BYTES;
  unsigned char * pk = malloc(pk_size);
  const int sk_size = SYS_T * 2;
  unsigned char * sk = malloc(sk_size);
  const int perm_size = (1 << GFBITS)*4;
  uint32_t * perm  = malloc(perm_size);

  // Init
  srand(time(NULL)); // Initialization, should only be called once.
  random_init((uint8_t*)pk,pk_size);
  random_init((uint8_t*)sk,sk_size);
  random_init((uint8_t*)perm,perm_size);

  // Wavefront compute-nex()
  int i, check=0;
  for (i=0;i<REPS;++i) {
    check += pk_gen(pk,sk,perm);
  }

  // Free
  free(pk);
  free(sk);
  free(perm);

  return 0;
}
